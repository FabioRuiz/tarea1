
import java.util.Scanner;

public class Punto3 {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Ingrese el n�mero de un mes  ");
			int mes= sc.nextInt();		
			while(mes < 1 || mes > 12) {
				System.out.println("El n�mero seleccionado no se encuentra dentro del rango predefinido, por favor ingrese otro n�mero" );
				System.out.println("Ingrese el n�mero de un mes ");
				 mes= sc.nextInt();
			}
			
			if(mes == 1) {
				System.out.println("El mes elegido es Enero y posee " + 31 + " dias");
			}
			if(mes == 2) {
				System.out.println("El mes elegido es Febrero y posee " + 28 + " dias");
			}
			if(mes == 3) {
				System.out.println("El mes elegido es Marzo y posee " + 31 + " dias");
			}
			if(mes == 4) {
				System.out.println("El mes elegido es Abril y posee " + 30 + " dias");
			}
			if(mes == 5) {
				System.out.println("El mes elegido es Mayo y posee " + 31 + " dias");
			}
			if(mes == 6) {
				System.out.println("El mes elegido es Junio y posee " + 30 + " dias");
			}
			if(mes == 7) {
				System.out.println("El mes elegido es Julio y posee " + 31 + " dias");
			}
			if(mes == 8) {
				System.out.println("El mes elegido es Agosto y posee " + 31 + " dias");
			}
			if(mes == 9) {
				System.out.println("El mes elegido es Septiembre posee " + 30 + " dias");
			}
			if(mes == 10) {
				System.out.println("El mes elegido es Octubre y posee " + 31 + " dias");
			}
			if(mes == 11) {
				System.out.println("El mes elegido es Noviembre y posee " + 30 + " dias");
			}
			if(mes == 12) {
				System.out.println("El mes elegido es Diciembre y posee " + 31 + " dias");
			}
			
			
	}

}
}