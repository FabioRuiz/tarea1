
import java.util.Scanner;

public class Punto1 {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Ingrese la primera nota del alumno ");
            int a= sc.nextInt();
            System.out.println("Ingrese la segunda nota del alumno ");
            int b= sc.nextInt();
            System.out.println("Ingrese la tercera nota del alumno ");
            int c= sc.nextInt();

            float x=a+b+c;
            float z = x/3; 
            if(z<7){
            System.out.println("El alumno tiene un promedio de notas de " + z + " por lo tanto esta desaprobado");
            }
            else {
                System.out.println("El alumno tiene un promedio de notas de " + z + " por lo tanto esta aprobado");
            }
        }
}
}