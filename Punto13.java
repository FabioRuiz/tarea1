import java.util.Scanner;

public class Punto13 {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
        	
        	System.out.println ("Ingrese el n�mero del mes");
        	int mes= sc.nextInt();		
			while(mes < 1 || mes > 12) {
				System.out.println("El n�mero seleccionado no se encuentra dentro del rango predefinido, por favor ingrese otro n�mero" );
				System.out.println("Ingrese el n�mero de un mes ");
				 mes= sc.nextInt();
			}
				 switch(mes) {
				 case 1 :
					 System.out.println ("El mes elegido es Enero y posee " + 31 +  " d�as");
					 break;
				 case 2 :
					 System.out.println ("El mes elegido es Febrero y posee " + 28 +  " d�as");
					 break;
				 case 3 :
					 System.out.println ("El mes elegido es Marzo y posee " + 31 +  " d�as");
					 break;
				 case 4 :
					 System.out.println ("El mes elegido es Abril y posee " + 30 +  " d�as");
					 break;
				 case 5 :
					 System.out.println ("El mes elegido es Mayo y posee " + 31 +  " d�as");
					 break;
				 case 6 :
					 System.out.println ("El mes elegido es Junio y posee " + 30 +  " d�as");
					 break;
				 case 7 :
					 System.out.println ("El mes elegido es Julio y posee " + 31 +  " d�as");
					 break;
				 case 8 :
					 System.out.println ("El mes elegido es Agosto y posee " + 31 +  " d�as");
					 break;
				 case 9 :
					 System.out.println ("El mes elegido es Septiembre y posee " + 30 +  " d�as");
					 break;
				 case 10 :
					 System.out.println ("El mes elegido es Octubre y posee " + 31 +  " d�as");
					 break;
				 case 11 :
					 System.out.println ("El mes elegido es Noviembre y posee " + 30 +  " d�as");
					 break;
				 case 12 :
					 System.out.println ("El mes elegido es Diciembre y posee " + 31 +  " d�as");
					 break;
				 }
        
        }
    }
}
    